/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f4xx.h"
#include "stm32f4xx_nucleo.h"
			
uint8_t x = 0;
uint8_t y = 0;
uint8_t z = 0;

void Delay( unsigned int x )
{
	while(x--);
}

int main(void)
{
	for(;;)
	{
		x += 15;
		y += 20;
		z += 80;
		Delay(1000000);
	}
}
